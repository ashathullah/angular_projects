import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.scss']
})
export class SideNavbarComponent implements OnInit {

  navItems = [
    {
      src: "../../../assets/image/logo.png",
      name: "Orders",
      link:"#"
    },
    {
      src: "../../../assets/image/logo.png",
      name: "Menu",
      link:"#"
    },
    {
      src: "../../../assets/image/logo.png",
      name: "Promotion",
      link:"#"
    },
    {
      src: "../../../assets/image/logo.png",
      name: "Reports",
      link:"#"
    },
    {
      src: "../../../assets/image/logo.png",
      name: "Metrics",
      link:"#"
    },
    {
      src: "../../../assets/image/logo.png",
      name: "Porfile",
      link:"#"
    },
    {
      src: "../../../assets/image/logo.png",
      name: "Help",
      link:"#"
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
