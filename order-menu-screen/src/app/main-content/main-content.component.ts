import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {

  //category details
categories = [
  {
    name: "Offer",
    link: "#",
    subcategories: [
      {
        name: "item 1",
        link: "#"
      },
      {
        name: "item 2",
        link: "#"
      },
      {
        name: "item 3",
        link: "#"
      },
      {
        name: "item 4",
        link: "#"
      }
    ]
  },
  {
    name: "Soup",
    link: "#",
    subcategories: [
      {
        name: "item 1",
        link: "#"
      },
      {
        name: "item 2",
        link: "#"
      },
      {
        name: "item 3",
        link: "#"
      },
      {
        name: "item 4",
        link: "#"
      }
    ]
  },
  {
    name: "Starter",
    link: "#",
    subcategories: [
      {
        name: "item 1",
        link: "#"
      },
      {
        name: "item 2",
        link: "#"
      },
      {
        name: "item 3",
        link: "#"
      },
      {
        name: "item 4",
        link: "#"
      }
    ]
  },
  {
    name: "Biryani",
    link: "#",
    subcategories: [
      {
        name: "item 1",
        link: "#"
      },
      {
        name: "item 2",
        link: "#"
      },
      {
        name: "item 3",
        link: "#"
      },
      {
        name: "item 4",
        link: "#"
      }
    ]
  },
  {
    name: "Main Course",
    link: "#",
    subcategories: [
      {
        name: "item 1",
        link: "#"
      },
      {
        name: "item 2",
        link: "#"
      },
      {
        name: "item 3",
        link: "#"
      },
      {
        name: "item 4",
        link: "#"
      }
    ]
  },
  {
    name: "Bread",
    link: "#",
    subcategories: [
      {
        name: "item 1",
        link: "#"
      },
      {
        name: "item 2",
        link: "#"
      },
      {
        name: "item 3",
        link: "#"
      },
      {
        name: "item 4",
        link: "#"
      }
    ]
  }
  
]

//product details
products = [
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},
{
  name:"Chicken briyani",
  price:"60",
  discounted: "60",
  notes:"extra spicy",
  quantity:"2"
},

]

toggleclass(idName,targetclass) {
  var target = document.getElementById(idName);
  if(target.className == targetclass + " hide") {
    target.className = targetclass;
  }
  else {
    target.className = targetclass + " hide";
  }
}



  constructor() { }

  ngOnInit(): void {
  }

}
